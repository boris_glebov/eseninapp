﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Linq;
using HtmlAgilityPack;

namespace Hqub.PrepareData
{
    class Program
    {
        static void Main(string[] args)
        {
            EseninFillDb();
        }

        private static void EseninFillDb()
        {

            HtmlWeb web = new HtmlWeb();
            web.AutoDetectEncoding = false;
            web.OverrideEncoding = Encoding.GetEncoding("WINDOWS-1251");

            var htmlDocument = web.Load("http://rupoem.ru/esenin/all.aspx");

            var poemDivs  = htmlDocument.DocumentNode.SelectNodes("//table//div");

            var xdoc = new XDocument();
            var xpoems = new XElement("poems");


            foreach (var poem in poemDivs)
            {
                var titleNode = poem.SelectSingleNode("h2[@class='poemtitle']");
                if (titleNode == null)
                    continue;

                var title = titleNode.InnerText;
                var poemyear = poem.SelectSingleNode("span[@class='poemyear']").InnerText;
                var text = poem.SelectSingleNode("pre").InnerText.Trim();
                var lines = text.Split('\n');

                var xpoem = new XElement("poem",
                    new XAttribute("Id", Guid.NewGuid()),
                    new XElement("title", title == "* * *" ? lines[0] : string.Format("{0}...", title)), 
                    new XElement("poemyear", poemyear));

                var xquatrains = new XElement("quatrains");
                xpoem.Add(xquatrains);

                var sb = new StringBuilder();
                
                foreach (var line in lines)
                {
                    if (line == "\r")
                    {
                        AddQuatrain(xquatrains, sb.ToString());
                        sb.Clear();
                    }

                    sb.AppendLine(line.Trim());
                }

                // Последнее четверостишие:
                AddQuatrain(xquatrains, sb.ToString());

                xpoems.Add(xpoem);
                
                Console.WriteLine("{0}\n\n{1}\n{2}", title, text, poemyear);
            }

            xdoc.Add(xpoems);
            xdoc.Save(@"esenin.xml");
            Console.WriteLine("\nend.");
            Console.ReadKey();
        }

        private static void AddQuatrain(XElement element, string text)
        {
            element.Add(new XElement("quatrain",
                           new XAttribute("Id", Guid.NewGuid()),
                           text));
        }
    }
}
