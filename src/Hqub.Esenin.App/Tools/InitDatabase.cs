﻿using System;
using System.IO;
using System.Windows;
using System.Windows.Resources;
using System.Xml.Linq;
using BrightstarDB.Client;
using BrightstarDB.Storage;
using Hqub.Esenin.App.Model;

namespace Hqub.Esenin.App.Tools
{
    public static class InitDatabase
    {
        private static MyEntityContext Context { get; set; }

        public static void Fill(MyEntityContext ctx)
        {
            if(ctx == null)
                return;

            Context = ctx;

            LoadEseninPoems();
        }

        public static bool Exists(string dbName)
        {
            var dataObjectContext = BrightstarService.GetDataObjectContext(Properties.Properties.BaseConnectionString);

            return dataObjectContext.DoesStoreExist(dbName);
        }

        public static void CreateDb(string dbName)
        {
            var dataObjectContext = BrightstarService.GetDataObjectContext(Properties.Properties.BaseConnectionString);

            if (!Exists(dbName))
            {
                dataObjectContext.CreateStore(dbName, persistenceType: PersistenceType.Rewrite);
            }
        }

        private static void LoadEseninPoems()
        {
            var xml =
              Application.GetResourceStream(new Uri("/Hqub.Esenin.App;component/Data/esenin.xml", UriKind.Relative));
            var appDataXml = XElement.Load(xml.Stream);

            foreach (var xpoem in appDataXml.Elements())
            {
                var poem = Context.Poems.Create();
                poem.Title = xpoem.Element("title").Value.Trim();
                poem.Year = xpoem.Element("poemyear").Value;

                var order = 0;
                foreach (var xquatrain in xpoem.Elements("quatrains").Elements())
                {
                    var quatrain = Context.Quatrains.Create();
                    quatrain.Order = ++order;
                    quatrain.Text = xquatrain.Value.Trim();

                    poem.Quatrains.Add(quatrain);
                }
            }

            Context.SaveChanges();
        }
    }
}
