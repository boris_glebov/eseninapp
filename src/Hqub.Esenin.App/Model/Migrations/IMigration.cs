﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Hqub.Esenin.App.Annotations;

namespace Hqub.Esenin.App.Model.Migrations
{
    public interface IMigration
    {
        void Up();
        string Version { get; }
    }
}
