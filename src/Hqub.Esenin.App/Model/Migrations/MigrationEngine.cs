﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Hqub.Esenin.App.Model.Migrations
{
    public static class MigrationEngine
    {
        private static Dictionary<string, IMigration> _migrations = new Dictionary<string, IMigration>
        {
            {"01", new Migration01()} 
        };

        public static void Run()
        {
            using (var context = DbContext.Create())
            {
                var m = context.Metadatas.FirstOrDefault();
                if (m == null)
                    _migrations["01"].Up();
           
            }
        }
    }
}
