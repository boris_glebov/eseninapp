﻿using System;
using System.Linq;
using BrightstarDB.Client;

namespace Hqub.Esenin.App.Model.Migrations
{
    public class Migration01 : IMigration
    {
        public void Up()
        {
            var dataObjectContext =
                BrightstarService.GetDataObjectContext(Properties.Properties.BaseConnectionString);
            try
            {
                // Удаляем старую БД
                if (dataObjectContext.DoesStoreExist(Properties.Properties.StoreName00))
                {
                    dataObjectContext.DeleteStore(Properties.Properties.StoreName00);
                }
            }
            catch (Exception ex)
            {
                System.Diagnostics.Debug.WriteLine(ex);
            }
            finally
            {
                // Создаем новую БД:
                Tools.InitDatabase.CreateDb(Properties.Properties.StoreName01);

                using (var context = DbContext.Create())
                {
                    if (context.Poems.Count() == 0)
                        Tools.InitDatabase.Fill(context);
                }
            }
        }

        public string Version
        {
            get { return "01"; }
        }
    }
}
