﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BrightstarDB.EntityFramework;
using Hqub.Esenin.App.Annotations;

namespace Hqub.Esenin.App.Model
{
    [Entity]
    public interface IMetadata
    {
        string Version { get; set; }
    }
}
