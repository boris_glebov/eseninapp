﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Hqub.Esenin.App.Model
{
    public partial class Poem
    {
        public int CompleatePercent
        {
            get
            {
                var compleatedCount = Quatrains.Count(q => q.Compleated);
                
                double percent = 0;
                percent = Math.Round(Quatrains.Count == 0 ? 0f : (compleatedCount/(Quatrains.Count*1.0))*100);

                return (int) percent;
            }
        }

        public string Description
        {
            get
            {
                var quatrain = Quatrains.FirstOrDefault(q=>q.Order == 1);
                if (quatrain == null)
                    return string.Empty;

                var lines = quatrain.Text.Split('\n');

                var description = lines.Length >= 3 ? string.Join("\n", lines[1], lines[2]) : quatrain.Text;

                return description;
            }
        }
    }
}
