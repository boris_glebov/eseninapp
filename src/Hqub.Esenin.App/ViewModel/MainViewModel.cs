﻿using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Windows.Input;
using GalaSoft.MvvmLight;
using GalaSoft.MvvmLight.Command;
using Hqub.Esenin.App.Code;
using Hqub.Esenin.App.Model;
using Microsoft.Phone.Shell;

namespace Hqub.Esenin.App.ViewModel
{
    public class MainViewModel : ViewModelBase
    {
        private ObservableCollection<IPoem> _poems;
        private List<AlphaKeyGroup<IPoem>> _poemDataSource;
        private IPoem _selectedPoem;
        private List<AlphaKeyGroup<IPoem>> _favoritePoemDataSource;
        private List<AlphaKeyGroup<IPoem>> _compleatedPoemDataSource;
        private string _searchText;
        private bool _searchMode;

        public MainViewModel()
        {
            LoadData();
        }

        #region Properties

        public bool IsDataLoad { get; set; }

        public ObservableCollection<IPoem> Poems
        {
            get { return _poems; }
            set
            {
                _poems = value; 
                RaisePropertyChanged();
            }
        }

        public List<AlphaKeyGroup<IPoem>> PoemDataSource
        {
            get { return _poemDataSource; }
            set
            {
                _poemDataSource = value;
                RaisePropertyChanged();
            }
        }

        public List<AlphaKeyGroup<IPoem>> FavoritePoemDataSource
        {
            get { return _favoritePoemDataSource; }
            set
            {
                _favoritePoemDataSource = value; 
                RaisePropertyChanged();
            }
        }

        public List<AlphaKeyGroup<IPoem>> CompleatedPoemDataSource
        {
            get { return _compleatedPoemDataSource; }
            set
            {
                _compleatedPoemDataSource = value;
                RaisePropertyChanged();
            }
        }

        public string SearchText
        {
            get { return _searchText; }
            set
            {
                _searchText = value;
                RaisePropertyChanged(() => SearchText);
            }
        }

        public IPoem SelectedPoem
        {
            get { return _selectedPoem; }
            set
            {
                _selectedPoem = value;
                RaisePropertyChanged();
            }
        }

        public bool SearchMode
        {
            get { return _searchMode; }
            set
            {
                _searchMode = value;
                RaisePropertyChanged();
            }
        }

        #endregion

        #region Commands

        public ICommand SearchCommand
        {
            get { return new RelayCommand(SearchCommandExecute); }
        }

        private void SearchCommandExecute()
        {
            SearchMode = !SearchMode;

            if (!SearchMode)
            {
                SearchText = string.Empty;
            }
        }

        #endregion

        /// <summary>
        /// Load poems from db
        /// </summary>
        public void LoadData()
        {
            using (var context = DbContext.Create())
            {
                IQueryable<IPoem> poems = context.Poems;
                poems = SetupFilter(poems);

                PoemDataSource = AlphaKeyGroup<IPoem>.CreateGroups(poems.ToList(),
                    System.Threading.Thread.CurrentThread.CurrentUICulture,
                    s => s.Title, true);

                var favPoems = context.Poems.Where(p => p.Bookmarked);
                favPoems = SetupFilter(favPoems);

                FavoritePoemDataSource = AlphaKeyGroup<IPoem>.CreateGroups(favPoems,
                    System.Threading.Thread.CurrentThread.CurrentUICulture,
                    s => s.Title, true);

                var compleatedPoems = context.Poems.Where(p => p.Compleated);
                compleatedPoems = SetupFilter(compleatedPoems);

                CompleatedPoemDataSource = AlphaKeyGroup<IPoem>.CreateGroups(compleatedPoems,
                    System.Threading.Thread.CurrentThread.CurrentUICulture,
                    s => s.Title, true);
            }
        }

        public void Clear()
        {
            SearchText = string.Empty;
        }

        private IQueryable<IPoem> SetupFilter(IQueryable<IPoem> query)
        {
            if (!string.IsNullOrEmpty(SearchText))
                query = query.Where(x => x.Title.ToLower().Contains(SearchText.ToLower()));

            return query;
        }

        private void SetProgressIndicator(bool isVisible)
        {
            SystemTray.ProgressIndicator.Text = "Ожидайте ...";
            SystemTray.ProgressIndicator.IsIndeterminate = isVisible;
            SystemTray.ProgressIndicator.IsVisible = isVisible;
        }

        public void Search()
        {
            SetProgressIndicator(true);

            LoadData();

            SearchMode = false;
            Clear();

            SetProgressIndicator(false);

        }
    }
}