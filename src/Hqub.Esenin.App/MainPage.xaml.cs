﻿using System;
using System.Diagnostics;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Input;
using System.Windows.Navigation;
using Hqub.Esenin.App.Model;
using Hqub.Esenin.App.Pages;
using Microsoft.Phone.Controls;
using Microsoft.Phone.Shell;

namespace Hqub.Esenin.App
{
    public partial class MainPage : ApplicationPageBase
    {
        // Constructor
        public MainPage()
        {
            InitializeComponent();

            DataContext = App.ViewModel;
        }

        protected override void OnNavigatedTo(NavigationEventArgs e)
        {
            base.OnNavigatedFrom(e);

            if (e.NavigationMode == NavigationMode.Back)
            {
                App.ViewModel.Clear();
                App.ViewModel.LoadData();
            }
        }

        private void LongListSelector_OnSelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            var poemList = sender as LongListSelector;

            if (poemList == null || poemList.SelectedItem == null)
                return;

            var poem = poemList.SelectedItem as IPoem;
            if (poem == null)
                return;
            
            // Reset selected item:
            poemList.SelectedItem = null;

            SetProgressIndicator(true);

            NavigationTo(new Uri(string.Format("/Pages/PoemPortraitPage.xaml?poemId={0}", poem.Id), UriKind.Relative));

            SetProgressIndicator(true);
        }

        private void ApplicationBarIconButton_OnClick(object sender, EventArgs e)
        {
            App.ViewModel.SearchCommand.Execute(null);
        }

        private void UIElement_OnKeyDown(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.Enter)
            {
                BindingExpression be = searchTextBox.GetBindingExpression(TextBox.TextProperty);
                be.UpdateSource();
                App.ViewModel.Search();
            }
        }

        private void SetProgressIndicator(bool isVisible)
        {
            SystemTray.ProgressIndicator.Text = "Ожидайте ...";
            SystemTray.ProgressIndicator.IsIndeterminate = isVisible;
            SystemTray.ProgressIndicator.IsVisible = isVisible;
        }

        private void MainPage_OnLoaded(object sender, RoutedEventArgs e)
        {
            SystemTray.ProgressIndicator = new ProgressIndicator();
        }
    }
}